# Lindel

Lindel is a Logistic regression model to predict insertions and deletions induced by CRISPR/Cas9 editing. 
This repository contains modified code from Wei Chen et.al biorxiv 2018 Paper, with the purpose to reproduce their results.  
This work was done for the TU Delft bioinformatics course CS4260 Machine Learning in Bioinformatics by the following students:  

Kirti Bihari  
Francesca Drummer  
Caroline Freyer  
Pia Keukeleire  

Using this tool requires ```numpy``` and ```scipy```.

To use the tool:

```
cd scripts
python Lindel_prediction.py your_input_sequence your_output_location
```

Change ```your_input_sequence``` to a 60 bp sequence centred at the cleavage site (e.g. ```TAACGTTATCAACGCCTATATTAAAGCGACCGTCGGTTGAACTGCGTGGATCAATGCGTC```), 
and change ```your_output_location``` to the directory where you want the output to be saved.