from scripts.LR.LR_combined import *
import scipy


def plot_figures():
    """
    Code used to plot the figures from the report. The function is separated in commented blocks to plot
    different figures. Since plotting can take some time, it is recommended to only uncomment the needed blocks. The
    first part of the function initializes variables needed for multiple figures.
    :return:
    """
    feature_index = "../../training_data/feature_index_all.pkl"

    training_our = "../../training_data/Our_training.txt"
    test_our = "../../training_data/Our_test.txt"
    l1_our = "../../trained_models/Our_L1.pkl"
    l2_our = "../../trained_models/Our_L2.pkl"
    weights_our = [pkl.load(open(l1_our, 'rb')), pkl.load(open(l2_our, 'rb'))]

    training_lindel = "../../training_data/Lindel_training.txt"
    test_lindel = "../../training_data/Lindel_test.txt"
    l1_lindel = "../../trained_models/Lindel_L1.pkl"
    l2_lindel = "../../trained_models/Lindel_L2.pkl"
    weights_lindel = [pkl.load(open(l1_lindel, 'rb')), pkl.load(open(l2_lindel, 'rb'))]

    _, _, features = pkl.load(open(feature_index, 'rb'))
    feature_size = len(features) + 384

    mses = []
    configs = [generate_configurations(test_our, feature_size, weights_our, 440, shuffle=False),
               generate_configurations(test_lindel, feature_size, weights_our, 440, shuffle=False),
               generate_configurations(test_our, feature_size, weights_lindel, 440, shuffle=False),
               generate_configurations(test_lindel, feature_size, weights_lindel, 440, shuffle=False)]

    # Make sure that the test sets are in the same order
    assert (configs[0][2] == configs[2][2]).all()

    agg_training_our = None

    for config in configs:
        _, y_hat, y_test = config
        mse_error = []
        for i in range(len(y_hat)):
            mse_error.append(((y_test[i] - y_hat[i]) ** 2).mean())
        mses.append(mse_error)

    ## HEATMAPS
    # agg_training_our = y_training(training_our, feature_size)
    # agg_test_our = y_training(test_our, feature_size)
    # agg_training_lindel = y_training(training_lindel, feature_size)
    # agg_test_lindel = y_training(test_lindel, feature_size)
    #
    # aggs = [agg_training_our, agg_test_our, agg_training_lindel, agg_test_lindel]
    #
    # for agg in aggs:
    #     plot_heat_map(agg)
    # print(scipy.stats.mannwhitneyu(agg_training_lindel, agg_training_our))
    # print(scipy.stats.mannwhitneyu(agg_test_our, agg_test_lindel))

    ## DATA OVERLAP
    # guides_training_our = set(load_data(training_our, feature_size)[2])
    # guides_test_our = set(load_data(test_our, feature_size)[2])
    # guides_training_lindel = set(load_data(training_lindel, feature_size)[2])
    # guides_test_lindel = set(load_data(test_lindel, feature_size)[2])
    #
    # print("In our training but not lindel training:", len(guides_training_our - guides_training_lindel))
    # print("In our test but not lindel test:", len(guides_test_our - guides_test_lindel))
    # print("In lindel training but not our training:", len(guides_training_lindel - guides_training_our))
    # print("In lindel test but not our test:", len(guides_test_lindel - guides_test_our))
    #
    # print("Interesting part:")
    # print("In our test and lindel training:", len(guides_test_our.intersection(guides_training_lindel)))
    # print("In our training and lindel test:", len(guides_training_our.intersection(guides_test_lindel)))

    ## 4 FULL HISTOGRAMS
    # if agg_training_our == None:
    #     agg_training_our = y_training(training_our, feature_size)
    # test(agg_training_our, test_our, feature_size, l1_our, l2_our, test_size=440)
    # print()
    # test(agg_training_our, test_lindel, feature_size, l1_our, l2_our, test_size=440)
    # print()
    # test(agg_training_our, test_our, feature_size, l1_lindel, l2_lindel, test_size=440)
    # print()
    # test(agg_training_our, test_lindel, feature_size, l1_lindel, l2_lindel, test_size=440)

    ## MSE AVG AND SD
    # for i in range(len(mses)):
    #     print(np.mean(mses[i]), np.std(mses[i]))

    ## ONE LINED HISTOGRAM
    # labels = ["Our train, our test", "Our train, Lindel test", "Lindel train, our test", "Lindel train, Lindel test"]
    # colors = ["deepskyblue", "m", "y", "lime"]
    #
    # fig, axs = plt.subplots(1, 1)
    # for i in range(len(mses)):
    #     axs.hist(mses[i], range=[0, 0.0014], bins=28, alpha=0.5, label=labels[i], ec=colors[i], histtype="step",
    #          lw=1)
    # plt.xlabel("MSE")
    # plt.ylabel("Count")
    # axs.set_ylim(0, 175)
    # axs.legend()
    # axs.ticklabel_format(axis="x", scilimits=(-3, -3))
    # plt.show()

    ## MSE DIFFERENCE
    # mse_diff_1 = [mses[0][i] - mses[2][i] for i in range(len(mses[0]))]
    # mse_diff_2 = [mses[1][i] - mses[3][i] for i in range(len(mses[1]))]
    # fig, axs = plt.subplots(1, 1)
    # axs.hist(mse_diff_1, range=[-1.1/1000, 0.75/1000], bins=70, alpha=0.5, histtype="stepfilled", label="Our test", color="skyblue", ec="skyblue",
    #          lw=0.2)
    # axs.hist(mse_diff_2, range=[-1.1/1000, 0.75/1000], bins=70, alpha=0.5, histtype="stepfilled", label="Lindel test", color="salmon", ec="salmon",
    #          lw=0.2)
    # plt.xlabel("MSE difference (our - Lindel)")
    # plt.ylabel("Count")
    # axs.set_xlim(-1.1/1000, 0.75/1000)
    # axs.legend()
    # axs.ticklabel_format(axis="x", scilimits=(-3, -3))
    # plt.show()

def plot_heat_map(y):
    plt.rcParams["figure.figsize"] = 5, 2
    x = list(range(557))
    fig, ax = plt.subplots(1,1)
    ax.set_ylim(0, 0.15)
    ax.set_xlim(0, 556)
    ax.plot(x, y)
    plt.xlabel("Output class")
    plt.ylabel("Output label")
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    plot_figures()