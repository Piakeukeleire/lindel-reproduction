import numpy as np
import pickle as pkl
from tqdm import tqdm
from keras.callbacks import EarlyStopping
from keras.layers import Dense, Input, Flatten
from keras.models import Sequential, load_model
from keras.regularizers import l2, l1


def onehotencoder(seq):
    """
    One hot encode the guide
    :param seq: guide
    :return: the encoded guide
    """
    nt = ['A', 'T', 'C', 'G']
    head = []
    l = len(seq)
    for k in range(l):
        for i in range(4):
            head.append(nt[i] + str(k))

    for k in range(l - 1):
        for i in range(4):
            for j in range(4):
                head.append(nt[i] + nt[j] + str(k))
    head_idx = {}
    for idx, key in enumerate(head):
        head_idx[key] = idx
    encode = np.zeros(len(head_idx))
    for j in range(l):
        encode[head_idx[seq[j] + str(j)]] = 1.
    for k in range(l - 1):
        encode[head_idx[seq[k:k + 2] + str(k)]] = 1.
    return encode


def mse(x, y):
    return ((x - y) ** 2).mean()


def load_data(training_data, feature_size, shuffle=True):
    """
    Load the training data
    :param training_data: txt file with input used for training
    :param feature_size: size of the features extracted from the feature_index
    :param shuffle: if True, the samples are shuffled
    :return: the input labels, the output labels and the guides
    """
    data = np.loadtxt(training_data, delimiter="\t", dtype=str)
    Seqs = data[:, 0]
    data = data[:, 1:].astype('float32')

    X = data[:, :feature_size]
    y = data[:, feature_size:]

    np.random.seed(121)
    idx = np.arange(len(y))
    if shuffle:
        np.random.shuffle(idx)
    X, y, Seqs = X[idx], y[idx], Seqs[idx]

    return X, y, Seqs


# Base for the regression models
class Model:

    def __init__(self):
        pass

    def train_model(self, training_data, feature_size, train_size, valid_size, regularizer="l1"):
        """
        Train the model
        :param training_data: txt file with input used for training
        :param feature_size: size of the features extracted from the feature_index
        :param train_size: number of training samples
        :param valid_size: number of validation samples
        :param regularizer: "l1" to use L1, "l2" to use L2 regularization
        :return: trained model
        """

        # Generate the data
        x_train, x_valid, y_train, y_valid, size_input = self.generate_train_valid(training_data, feature_size, train_size, valid_size)

        # Test out different lambdas for regularization
        lambdas = 10 ** np.arange(-10, -1, 0.1)
        errors = []
        for l in tqdm(lambdas):
            np.random.seed(0)
            r = l1(l) if regularizer == "l1" else l2(l)
            model = self.create_model(size_input, r)
            model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['mse'])
            model.fit(x_train, y_train, epochs=100, validation_data=(x_valid, y_valid),
                      callbacks=[EarlyStopping(patience=1)], verbose=0)
            y_hat = model.predict(x_valid)
            errors.append(mse(y_hat, y_valid))

        # Choose the best lambda
        l = lambdas[np.argmin(errors)]
        np.random.seed(0)
        r = l1(l) if regularizer == "l1" else l2(l)
        model = self.create_model(size_input, r)
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['mse'])
        model.fit(x_train, y_train, epochs=100, validation_data=(x_valid, y_valid),
                            callbacks=[EarlyStopping(patience=1)], verbose=0)
        return model

    def dense_layer(self, size_input):
        pass

    def create_model(self, size_input, r=None):
        model = Sequential()
        dense = self.dense_layer(size_input)
        if r is not None:
            dense.kernel_regularizer = r
        model.add(dense)
        return model

    def generate_train_valid(self, training_data, feature_size, train_size, valid_size):
        """
        Generate the training/validation data
        :param training_data: txt file with input used for training
        :param feature_size: size of the features extracted from the feature_index
        :param train_size: number of training samples
        :param valid_size: number of validation samples
        :return: train input, validation input, train output, validation output
        """
        X, y, Seqs = load_data(training_data, feature_size)
        x_train, x_valid = [], []
        y_train, y_valid = [], []

        # Generate training data
        for i in range(train_size):
            xy = self.generate_x_y(X[i], y[i], Seqs[i])
            if xy is not None:
                y_train.append(xy[0])
                x_train.append(xy[1])

        # Generate validation data
        for i in range(train_size, min(train_size + valid_size, len(Seqs))):
            xy = self.generate_x_y(X[i], y[i], Seqs[i])
            if xy is not None:
                y_valid.append(xy[0])
                x_valid.append(xy[1])

        x_train, x_valid = np.array(x_train), np.array(x_valid)
        y_train, y_valid = np.array(y_train), np.array(y_valid)
        size_input = x_train.shape[1]

        return x_train, x_valid, y_train, y_valid, size_input

    def generate_x_y(self, x, y, seq):
        pass


class InDelModel(Model):

    def __init__(self):
        super().__init__()

    def dense_layer(self, size_input):
        return Dense(2, activation='softmax', input_shape=(384,))

    def generate_x_y(self, x, y, seq):
        return (sum(y[:-21]),sum(y[-21:])), onehotencoder(seq)


class DeletionModel(Model):

    def __init__(self):
        super().__init__()

    def dense_layer(self, size_input):
        return Dense(536, activation='softmax', input_shape=(size_input,))

    def generate_x_y(self, x, y, seq):
        if 1 > sum(y[:536]) > 0:
            return y[:536] / sum(y[:536]), x
        return None


class InsertionModel(Model):

    def __init__(self):
        super().__init__()

    def dense_layer(self, size_input):
        return Dense(21, activation='softmax', input_shape=(size_input,))

    def generate_x_y(self, x, y, seq):
        if 1 > sum(y[-21:]) > 0:
            return y[-21:] / sum(y[-21:]), onehotencoder(seq[-6:])
        return None
