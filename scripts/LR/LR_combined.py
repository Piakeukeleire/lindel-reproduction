import time
import os
import matplotlib.pyplot as plt
from scripts.LR.LR_model import *


def train(training_data, feature_size, model, reg):
    """
    Train the model.
    :param training_data: txt file with input used for training
    :param feature_size: size of the features extracted from the feature_index
    :param model: name of the file where to save the model
    :param reg: "l1" to use L1 regularization, "l2" to use L2 regularization
    :return:
    """
    # Use a 90 - 10 split
    train_size = 3876
    valid_size = 431
    start_time = time.time()

    # Train the 3 separate models
    model_indel = InDelModel().train_model(training_data, feature_size, train_size, valid_size, reg)
    weights_indel = model_indel.get_weights()

    model_del = DeletionModel().train_model(training_data, feature_size, train_size, valid_size, reg)
    weights_del = model_del.get_weights()

    model_in = InsertionModel().train_model(training_data, feature_size, train_size, valid_size, reg)
    weights_in = model_in.get_weights()

    # Create an array contain all the weights of the 3 models
    # Sequence is [indel, del, in]
    weights = [weights_indel[0], weights_indel[1], weights_del[0], weights_del[1], weights_in[0], weights_in[1]]

    # Save the model
    os.makedirs("../../trained_models", exist_ok=True)
    with open(model, "wb") as f:
        pkl.dump(weights, f)
    print(time.time() - start_time)


def test(y_train, test_data, feature_size, model_l1, model_l2, plot=None, test_size=None):
    """
    Test the model and plot a histogram of the MSE losses.
    :param y_train: the output of the aggregate model
    :param test_data: txt file with input used for testing
    :param feature_size: size of the features extracted from the feature_index
    :param model_l1: path containing the weights of a model trained with l1 regularization
    :param model_l2: path containing the weights of a model trained with l2 regularization
    :param plot: None to choose best configuration, "l1" to use L1 regularization, l2 to use L2 regularization
    :param test_size: samples to use for testing, if None, all samples are used
    :return:
    """
    # Load the weights
    weights = [pkl.load(open(model_l1, 'rb')), pkl.load(open(model_l2, 'rb'))]

    # Generate the best configuration
    y_hats, best_y, y_test = generate_configurations(test_data, feature_size, weights, test_size)

    # Override best configuration if necessary
    if plot == "l1":
        best_y = y_hats[0]
    elif plot == "l2":
        best_y = y_hats[3]

    # Calculate the MSE errors for the Lindel and aggregate models
    mse_error_agg = [((y_test[i] - y_train)**2).mean() for i in range(len(best_y))]
    mse_error_lindel = [((y_test[i] - best_y[i])**2).mean() for i in range(len(best_y))]

    # Plot the two histograms in one figure, use bins=28 to resemble fig 6B the most
    fig, axs = plt.subplots(1, 1)
    axs.hist(mse_error_lindel, range=[0, 0.0014], bins=28, alpha=0.5, label="Lindel", color="skyblue", ec="skyblue", lw=0.1)
    axs.hist(mse_error_agg, range=[0, 0.0014], bins=28, alpha=0.5, label="Aggregate model", color="salmon", ec="white", lw=0.1)
    plt.xlabel("MSE")
    plt.ylabel("Count")
    axs.set_ylim(0, 175)
    axs.legend()
    axs.ticklabel_format(axis="x", scilimits=(-3,-3))
    plt.show()


def generate_configurations(test_data, feature_size, weights, test_size=None, shuffle=True, ret_weights=False):
    """
    Generate the best regularization configuration for the given weights.
    :param test_data: txt file with input used for testing
    :param feature_size: size of the features extracted from the feature_index
    :param weights: array containing the concatenated weights of the L1 and L2 trained model
    :param test_size: samples to use for testing, if None, all samples are used
    :param shuffle: if True, the test samples are shuffled, thus the order outputs cannot be guaranteed
    :return (y_hats, best_y, y_test): y_hats - the output of all configurations, best_y - the output of
    the best configuration, y_test - the output of the aggregate model
    """
    X, y, Seqs = load_data(test_data, feature_size, shuffle)
    y_test = []
    x_indel = []
    x_del = []
    x_in = []

    # Create the input per model
    for i in range(len(Seqs)):
        if 1 > sum(y[i, :536]) > 0 and 1 > sum(y[i, -21:]) > 0:
            if test_size is None or len(y_test) < test_size:
                x_indel.append(onehotencoder(Seqs[i]))
                x_del.append(X[i])
                x_in.append(onehotencoder(Seqs[i][-6:]))
                y_test.append(y[i])
    x_indel = np.array(x_indel)
    x_del = np.array(x_del)
    x_in = np.array(x_in)
    y_test = np.array(y_test)

    # Create the models
    model_indel = InDelModel().create_model(x_indel.shape[1])
    model_del = DeletionModel().create_model(x_del.shape[1])
    model_in = InsertionModel().create_model(x_in.shape[1])

    best_mse_error = 100
    best_y = []

    # Generate all outputs
    y_indels = []
    model_indel.set_weights(weights[0][:2])
    y_indels.append(np.array(model_indel.predict(x_indel)))
    model_indel.set_weights(weights[1][:2])
    y_indels.append(np.array(model_indel.predict(x_indel)))
    weights_indels = [weights[0][:2], weights[1][:2]]

    y_dels = []
    model_del.set_weights(weights[0][2:4])
    y_dels.append(np.array(model_del.predict(x_del)))
    model_del.set_weights(weights[1][2:4])
    y_dels.append(np.array(model_del.predict(x_del)))
    weights_dels = [weights[0][2:4], weights[1][2:4]]

    y_ins = []
    model_in.set_weights(weights[0][4:])
    y_ins.append(np.array(model_in.predict(x_in)))
    model_in.set_weights(weights[1][4:])
    y_ins.append(np.array(model_in.predict(x_in)))
    weights_ins = [weights[0][4:], weights[1][4:]]

    # Calculate the MSE and store the best output
    y_hats = []
    best_weights = []
    for weights_indel, y_indel in zip(weights_indels, y_indels):
        for weights_del, y_del in zip(weights_dels, y_dels):
            for weights_in, y_in in zip(weights_ins, y_ins):
                y_hat = []
                for i in range(len(y_indel)):
                    y_hat.append(np.concatenate((y_del[i] * y_indel[i, 0], y_in[i] * y_indel[i, 1])))
                y_hats.append(y_hat)
                mse_error = ((y_test - y_hat) ** 2).mean()
                if mse_error < best_mse_error:
                    best_mse_error = mse_error
                    best_y = y_hat
                    best_weights = [weights_indel[0], weights_indel[1], weights_del[0], weights_del[1], weights_in[0], weights_in[1]]
                # Print the MSE of this configuration
                # print(round(mse_error * 1000, 4))
    if ret_weights:
        return best_weights
    return y_hats, best_y, y_test


def write_best_weights(test_data, feature_size, l1_model, l2_model, model):
    """
    Calculate the best weight configuration and write them to a file
    :param test_data: txt file with input used for testing
    :param feature_size: size of the features extracted from the feature_index
    :param l1_model: path to Lindel model trained using L1 regularization
    :param l2_model: path to Lindel model trained using L2 regularization
    :param model: destination path of best weights
    """
    weights_lindel = [pkl.load(open(l1_model, 'rb')), pkl.load(open(l2_model, 'rb'))]
    best_weights = generate_configurations(test_data, feature_size, weights_lindel, ret_weights=True)
    # Save the model
    os.makedirs("../../trained_models", exist_ok=True)
    with open(model, "wb") as f:
        pkl.dump(best_weights, f)


def y_training(training_data, feature_size, norm=True):
    """
    Calculate the average output, used as the prediction for the aggregate model
    :param training_data: txt file with input used for training
    :param feature_size: size of the features extracted from the feature_index
    :param norm: if False, undo the multiplying of the outputs with the indel ratios
    :return: output of aggregate model, or average of training sample outputs
    """
    X, y, Seqs = load_data(training_data, feature_size)
    y_train = []

    for i in range(len(Seqs)):
        if 1 > sum(y[i, :536]) > 0 and 1 > sum(y[i, -21:]) > 0:
            if norm:
                y_train.append(y[i])
            elif not norm:
                y_train.append(np.concatenate((y[i, :536] / sum(y[i, :536]), y[i, -21:] / sum(y[i, -21:]))))
    y_train = np.array(y_train)
    y_test = y_train.mean(axis=0)
    return y_test


def sequence_dict(algient):
    """
    Create a dictionary that maps from a guide to the full sequence, used to collapse classes
    :param algient: path to the algient_NHEJ_guides_final.txt file
    :return:
    """
    with open(algient) as f:
        data = f.readlines()

    sequences = {}
    for seq in data:
        seq = seq.strip()
        design = "70k seq design"
        if seq[-len(design):] == design:
            guide = seq[20:40]
            sequences[guide] = seq
    return sequences


if __name__ == "__main__":
    feature_index = "../../training_data/feature_index_all.pkl"

    training_data = "../../training_data/Our_training.txt"
    test_data = "../../training_data/Our_test.txt"

    train_model = "../../trained_models/trained_weights.pkl"
    l1_model = "../../trained_models/Our_L1.pkl"
    l2_model = "../../trained_models/Our_L2.pkl"

    _, _, features = pkl.load(open(feature_index, 'rb'))
    feature_size = len(features) + 384

    # Calculate aggregate model output
    # agg = y_training(training_data, feature_size)

    # Train the model and save to the train_model file
    train(training_data, feature_size, train_model, "l2")

    # Test the model
    # test(agg, test_data, feature_size, l1_model, l2_model)

    # Write best weights to file
    # write_best_weights(test_data, feature_size, l1_model, l2_model, train_model)
