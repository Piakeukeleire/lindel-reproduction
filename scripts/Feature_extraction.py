import pickle as pkl
import numpy as np
import os
import random
from Lindel_prediction import *
import csv


def soft_label_calculation(data_file, label_file):
    """
    :param data_file: text file containing the modelling indels for each guide
    :param label_file: text file containing the softlabels labels
    :return: softlabel.txt containing the guides and all softlabels
    """
    data = open(data_file, 'r')
    # Extract position indices into array
    labels_file = open(label_file, 'r')
    pos_labels = []
    for line in labels_file:
        pos, idx = line.split(": ")
        idx = idx[:-1]
        if int(idx) < 556:
            start_id, length = pos.split("+")
            pos_labels.append([start_id, length])
        else:
            pos_labels.append([3])
    labels = {}
    for line in data:
        line = line.split("\t")
        guide = str(line[0])
        rest = [s[1:-1].split(' ') for s in line[2:]]
        total = 1
        filtered_ins = []
        filtered_del = []
        # filter items
        for item in rest:
            # filter on length (<30 bp) and no overlap -3/+2 window around cleavage side
            if item[1] != "None" and item[2] != "None":
                overlap = int(item[1]) + int(item[2])
                if int(item[2]) < 30 and overlap > -2:
                    # remove items with wildtypes or complex labels
                    if item[0] == "'ins'":
                        filtered_ins.append(item)
                        total += 1
                    elif item[0] == "'del'":
                        filtered_del.append(item)
                        total += 1
        # calculate the softlabels
        labels[guide] = []
        for i, ele_list in enumerate(pos_labels.copy()):
            count = 0
            if i < 536:  # deletions and 1bp & 2bp insertions
                start_id, length = ele_list
                for item in filtered_del:
                    if item[1] == start_id and item[2] == length:
                        count += 1
                labels[guide].append(count / total)
            elif i < 556:  # 1bp & 2bp insertions
                start_id, nuceleotide = ele_list
                for item in filtered_ins:
                    sign = item[3][1:1 + int(item[2])]
                    if sign == nuceleotide:
                        count += 1
                labels[guide].append(count / total)
            else:  # others
                for item in filtered_ins:
                    if item[3] != "None" and item[0] == "'ins'":
                        if len(item[3]) > 4:  # 3 because also contains ': 'N'
                            count += 1
                labels[guide].append(count / total)

    with open("../data/softlabels.txt", 'w') as f:
        for key, value in labels.items():
            f.write(str(key) + ": " + str(value) + '\n')

    return labels


def get_model_indels(guides, all_rep, out):
    """
    Matches the indels to each guide from the training and test data of Lindel
    :param guides: set containing all guides from Lindel test and training set
    :param all_rep: NHEJ_allrep_final_matrix.pkl
    :param out: modelling_indels.txt
    :return:
    """
    indels = pkl.load(open(all_rep, 'rb'))
    final_indels = {k: [] for k in guides}
    out = open(out, 'w')
    for indel in indels:
        if indel[6] in guides:
            final_indels[indel[6]].append(indel[9:])
    for guide, indel in final_indels.items():
        out.write(guide + '\t' + '\t'.join([str(x) for x in indel]) + '\n')


def calc_correlations(rep1, rep2, rep3, guides):
    """
    Calculates the correlation between 3 replicates by doing pairwise correlation calculations and taking the smallest.
    :param guides: all guides whose correlation between replicates we calculate
    :param rep1: replicate 1
    :param rep2: replicate 2
    :param rep3: replicate 3
    :return: the correlations for all guides
    """
    guides = open(guides, 'r')
    guides = [x.strip() for x in guides.readlines()]
    corrs = {k: 0 for k in guides}
    indels1 = get_counts(rep1, guides)
    indels2 = get_counts(rep2, guides)
    indels3 = get_counts(rep3, guides)
    for g in guides:
        if sum(indels1[g]) < 10 or sum(indels2[g]) < 10 or sum(indels3[g]) < 10:
            corrs[g] = 0
        else:
            r1 = correlation(indels1[g], indels2[g])
            r2 = correlation(indels2[g], indels3[g])
            r3 = correlation(indels1[g], indels3[g])
            corrs[g] = min(r1, r2, r3)
    return corrs


def correlation(rep1, rep2):
    """
    Calculates Pearson's r between a guide from rep1 and rep2.
    :param rep1: guide from replicate 1
    :param rep1: guide from replicate 2
    :return: returns Pearson's r
    """
    # right now only insertion and deletion counts are used
    m1 = (rep1[0] + rep1[1] + rep1[2]) / 3
    m2 = (rep2[0] + rep2[1] + rep2[2]) / 3
    r = 0
    vars = [0, 0]
    for i in 0, 1, 2:  # loop over insertion and deletion outcomes
        r += (rep1[i] - m1) * (rep2[i] - m2)
        vars[0] += (rep1[i] - m1) ** 2
        vars[1] += (rep2[i] - m2) ** 2
    if not vars[0] == 0 and not vars[1] == 0:
        r /= (np.sqrt(vars[0]) * np.sqrt(vars[1]))
    else:
        r = 0
    return r


def get_counts(rep, guides):
    """
    Counts the number of times a specific guide in rep data results in
    either a wildtype (WT), deletion (del), insertion (ins) or complex modification"
    :param rep: pkl file containing replicates
    :param guides: txt file containing all unique guides
    :return: dictionary with counts for each modification occurence
    """
    rep = pkl.load(open(rep, 'rb'))
    idx = {'WT': 0, 'del': 1, 'ins': 2, 'complex': 3}
    indels = {k: [0, 0, 0, 0] for k in guides}
    for x in rep:
        indels[x[6]][idx[x[9]]] += 1
    return indels


def get_all_guides(all_rep, out):
    """
    Saves all unique guides from all_rep file and to the out file.
    :param all_rep: input file NHEJ_allrep_final_matrix.pkl (Table of 14 x 1224151; contains guides at column 6)
    :param out: output file all_guides.txt
    """
    guides = set()
    indels = pkl.load(open(all_rep, 'rb'))
    out = open(out, 'w')
    for x in indels:
        guides.add(x[6])
    out.write('\n'.join(guides))


def get_seqs_from_guides(guides, seq_file):
    """
    Retrieves the full 60bp input sequence from the guides.
    This sequence is later used for obtaining the input features.
    :param guides: all input guides used for modeling
    :param seq_file: the file containing all the full sequences
    :return: the input sequences
    """
    guides = set(guides)
    print(len(guides))
    seqs = open(seq_file, 'r')
    seqs = seqs.readlines()
    # Use only 70k seq design sequences
    seqs = seqs[1061:70003]
    res = []
    for s in seqs:
        design = "70k seq design"
        if s[20:40] in guides and s[-len(design) - 1:-1] == design:
            start = s.index(s[20:40], 40) - 13
            res.append(s[start:start + 60])  # [129:189])
    return res


def write_input_file(seqs, train_file, test_file, prereq, softlabels):
    """
    Obtains the features from the input sequences and writes the guide and the features to the out file.
    :param seqs: the input sequences
    :param train_file: output file location for training data
    :param train_file: output file location for testing data
    :param prereq: contains the features
    :param softlabels: dictionary with all guides and their respective softlabels
    """
    prereq = pkl.load(open(prereq, 'rb'))
    f_train = open(train_file, 'w')
    f_test = open(test_file, 'w')
    test = set(random.sample(seqs, int(0.1 * len(seqs))))
    for seq in seqs:
        guide = seq[13:33]
        label, rev_index, features, frame_shift = prereq
        indels = gen_indel(seq, 30)
        input_indel = onehotencoder(guide)
        input_del = np.concatenate((create_feature_array(features, indels), input_indel), axis=None)
        if seq in test:
            out = f_test
        else:
            out = f_train
        out.write(
            guide + '\t' + '\t'.join([str(x) for x in input_del]) + '\t' +
            '\t'.join([str(label) for label in softlabels[guide]]) + '\n')


if __name__ == "__main__":
    all_rep = "../data/NHEJ_allrep_final_matrix.pkl"
    rep1 = "../data/NHEJ_rep1_final_matrix.pkl"
    rep2 = "../data/NHEJ_rep2_final_matrix.pkl"
    rep3 = "../data/NHEJ_rep3_final_matrix.pkl"
    model_indels = "../data/modeling_indels.txt"
    all_guides = "../data/all_guides.txt"
    rep_corrs = "../data/corrs_across_reps.txt"
    all_seqs = "../data/algient_NHEJ_guides_final.txt"
    new_train = "../training_data/Our_training.txt"
    new_test = "../training_data/Our_test.txt"
    prereq = "../training_data/Lindel_model_prereq.pkl"
    guides_type = "../data/guides_type.txt"
    labels = "../data/labels.txt"
    softlabels = "../data/softlabels.txt"

    # Merge the 3 replicates
    if not os.path.isfile(all_rep):
        rep1 = pkl.load(open(rep1, 'rb'))
        rep2 = pkl.load(open(rep2, 'rb'))
        rep3 = pkl.load(open(rep3, 'rb'))
        rep = np.concatenate((rep1, rep2))
        rep = np.concatenate((rep, rep3))
        with open(all_rep, "wb") as f:
            pkl.dump(rep, f)
    if not os.path.isfile(all_guides):
        get_all_guides(all_rep, all_guides)

    # calculate the correlations between the replicates
    corrs = calc_correlations(rep1, rep2, rep3, all_guides)

    # Uncomment this to write the correlations to a file
    # with open(rep_corrs, 'w') as out:
    #     for x in corrs:
    #         out.write(x + ': ' + str(corrs[x]) + '\n')

    # use only the guides that have a corr > 0.75
    guides = [x for x in corrs if corrs[x] > 0.75
              ]
    # obtain the indel data for these guides from all 3 replicates, and write this info to modeling_indels
    if not os.path.isfile(model_indels):
        get_model_indels(guides, all_rep, model_indels)
    if not os.path.isfile(softlabels):
        softlabels = soft_label_calculation(model_indels, labels)
    else:
        sl = open(softlabels, 'r')
        softlabels = {x.split(':')[0]: [float(y) for y in x.split(': ')[1][1:-2].split(', ')] for x in sl.readlines()}

    # obtain the input sequences from the algient_NHEJ_guides_final
    seqs = get_seqs_from_guides(guides, all_seqs)
    print(len(seqs))

    # write features of these input seqs to a file
    write_input_file(seqs, new_train, new_test, prereq, softlabels)
